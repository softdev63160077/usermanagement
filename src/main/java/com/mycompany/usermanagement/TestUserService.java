/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

/**
 *
 * @author NonWises
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService.addUser("users", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUsers());
        
        User user = UserService.getUser(3);
        System.out.println(UserService.getUsers());
        
        user.setPassword("1234");
        UserService.updateUser(3, user);
        System.out.println(UserService.getUsers());
        
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
        UserService.login("admin", "password");
        
    }
}
