/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author NonWises
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    // Mockup
    static {
        userList.add(new User("admin", "password"));
        
        
    }
    
    // Create
    public static boolean addUser(User user){
        if (user.getUserName().equals("admin")) {
            JOptionPane.showMessageDialog(null, "username admin already existed.");
            return false;
        }
        userList.add(user);
        return true;
    }
    public static boolean addUser(String userName, String password){
        userList.add(new User(userName, password));
        return true;
    }
    
    public static boolean updateUser(int index, User user){
        userList.set(index, user);
        return true;
    }
    // Read 1 user
    public static User getUser(int index){
        if (index>userList.size()-1) {
            return null;
        }
        return userList.get(index);
    }
    
    // Read all user
   public static ArrayList<User> getUsers(){
        return userList;
    }
    // Search username
     public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
                        
            }
        }
        return list;
    }
    // Delete user
    public static boolean delUser(int index)  {
        userList.remove(index);
        return true;
    }
    // Delete user
    public static boolean delUser(User user)  {
        userList.remove(user);
        return true;
    }
    // Login
    public static User login(String UserName, String password){
        for (User user : userList) {
            if (user.getUserName().equals(UserName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
    
    public static void save(){
        FileOutputStream fos = null;
        try {
            File file = new File("users.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(getUsers());
//            for (User user1 : userList) {
//                oos.writeObject(user1);
//            }
            System.out.println(getUsers());
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void load(){
        FileInputStream fis = null;
        try {
            File file = new File("users.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            System.out.println(getUsers());
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis!=null) {
                   fis.close(); 
                }
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
